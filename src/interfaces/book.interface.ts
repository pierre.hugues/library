/**
 * Interface for Book
 */
export interface Book {
    id: Number;
    title: String;
    genre: String;
    publication: Date;
    shelfMark: String;
    idAuthor: Number;
    idLibrary: Number;
}
