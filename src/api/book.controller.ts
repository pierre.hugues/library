// Import express
import express, {Request, Response, NextFunction} from 'express';
// Import interface defining a book
import {Book} from '../interfaces/book.interface';

// Import and instantiate Service for book
import BookService from '../services/book.service';

const bookService = new BookService();

// Create router
export const bookController = express.Router({
  mergeParams: true,
});

// Get all books
bookController.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const idLibrary: String = req.params.idLibrary;
    const libraries: Book[] = await bookService.getAll(idLibrary);
    return res.status(200).send(libraries);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Get a single book
bookController.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const idLibrary: String = req.params.idLibrary;
    const idBook: String = req.params.id;
    const book: Book = await bookService.getOne(idLibrary, idBook);
    return res.status(200).send(book);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Create a book
bookController.post('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const idLibrary: String = req.params.idLibrary;
    const book: Book = req.body;
    const idBook: Number = await bookService.create(idLibrary, book);
    return res.status(201).send(`Created with id : ${idBook}`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Update a book
bookController.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const idLibrary: String = req.params.idLibrary;
    const idBook: String = req.params.id;
    const book: Book = req.body;
    await bookService.update(idLibrary, idBook, book);
    return res.status(200).send(`Updated`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});
