import express, {Request, Response, NextFunction} from 'express';

export const defaultController = express.Router();

defaultController.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.status(200).send();
});
