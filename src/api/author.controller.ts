// Import express
import express, {Request, Response, NextFunction} from 'express';
// Import interface defining an author
import {Author} from '../interfaces/author.interface';

// Import and instantiate Service for author
import AuthorService from '../services/author.service';

const authorService = new AuthorService();

// Create router
export const authorController = express.Router();

// Get all authors
authorController.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const libraries: Author[] = await authorService.getAll();
    return res.status(200).send(libraries);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Get a single author
authorController.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const author: Author = await authorService.getOne(req.params.id);
    return res.status(200).send(author);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Create an author
authorController.post('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const author: Author = req.body;
    const idAuthor: Number = await authorService.create(author);
    return res.status(201).send(`Created with id : ${idAuthor}`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});

// Update an author
authorController.put('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const author: Author = req.body;
    const id: String = req.params.id;
    await authorService.update(author, id);
    return res.status(200).send(`Updated`);
  } catch (error: any) {
    // Transit error to error controller
    next({
      status: error.status || 500,
      message: error.message,
    });
  }
});
