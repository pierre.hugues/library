// Import interface defining an author
import {Author} from '../interfaces/author.interface';

// Import and instantiate Model for author
import AuthorModel from '../models/author.model';

const authorModel = new AuthorModel();

/**
 * Service dealing with Authors
 */
export default class AuthorService {
  /**
   * Find all authors
   * @return {Promise<Author[]>} a Promise containing an array of Authors
   */
  getAll(): Promise<Author[]> {
    return authorModel.getAll();
  }

  /**
   * Find a precise author
   * @param {String} id Id of author to find
   * @return {Promise<Author>} a Promise containing the Author searched
   */
  getOne(id: String): Promise<Author> {
    return authorModel.getOne(id);
  }

  /**
   * Create an author
   * @param {Author} author Author to create
   * @return {Promise<Number>} a Promise containing the ID of the Author created
   */
  create(author: Author): Promise<Number> {
    return authorModel.create(author);
  }

  /**
   * Update an author
   * @param {Author} author New values for author
   * @param {String} id ID of author to update
   * @return {Promise<void>} a Promise empty
   */
  update(author: Author, id: String): Promise<void> {
    return authorModel.update(author, id);
  }
}
