CREATE TABLE author
(
  id_author SERIAL NOT NULL,
  firstname varchar(45) NOT NULL,
  name varchar(45) NOT NULL,
  PRIMARY KEY (id_author)
);

CREATE TABLE IF NOT EXISTS public.library
(
  id_library SERIAL NOT NULL,
  address varchar(100),
  name varchar(45),
  PRIMARY KEY (id_library)
);

CREATE TABLE book (
  id_book SERIAL NOT NULL,
  title varchar(45) NOT NULL,
  genre varchar(45) NOT NULL,
  shelf_mark varchar(45) NOT NULL,
  publication timestamp with time zone NOT NULL,
  id_author int NOT NULL,
  id_library int NOT NULL,
  PRIMARY KEY (id_book),
  CONSTRAINT fk_book_1 FOREIGN KEY (id_author) REFERENCES author (id_author) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_book_2 FOREIGN KEY (id_library) REFERENCES library (id_library) ON DELETE CASCADE ON UPDATE CASCADE
);